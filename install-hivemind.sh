#!/bin/bash

echo "Checking if config folder exists"

WHOAMI=`logname`

PREFDIR="/Users/$WHOAMI/Library/Preferences/hivemind"

ACTION=$1

if [[ -z $ACTION || $ACTION != "update" ]]; then
	echo "Moving Executable (may need to run as sudo)"
	cp hivemind.sh "/usr/local/bin/hivemind"
	chmod u+rx /usr/local/bin/hivemind
	chown -R $WHOAMI /usr/local/bin/hivemind
	
	if [ -d "$PREFDIR" ]; then
		echo "Config Folder exists."
	else
		echo "Creating Configuration folder $PREFDIR"
		mkdir "$PREFDIR"
		touch "$PREFDIR/.library"
	fi

fi

echo "Moving templates in config dir"
cp -r "./templates" "$PREFDIR"

echo "Making sure permissions are okay"
chown -R $WHOAMI "$PREFDIR"  
exit
