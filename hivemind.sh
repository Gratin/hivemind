#!/bin/sh
#DIR = folder form which the function is called
DIR="$(pwd)"
INSTALLDIR=`which hivemind`
WHOAMI=`whoami`
PREFERENCESDIR="/Users/$WHOAMI/Library/Preferences/hivemind"
skipInteractive=$1
interactive='n'

clear

######################################
#######						 #########
#######	     FUNCTIONS		 #########
#######						 #########
######################################


##############################
######## I/O Manipulation
##############################
write_file()
{
	CONTENT=$1
	FILE=$2
	echo $CONTENT >> $FILE
}

write_site_env()
{
	KEY=$1
	VALUE=$2
	write_file "$KEY=$VALUE" $CONFIGFILE
	source $CONFIGFILE
}

replace_app_port()
{
	port=$1
	replace "s+%APPPORT%+$port+" docker-compose.yml
}

replace_app_root()
{
	root=$1
	replace "s+%APPROOT%+$root+" docker-compose.yml
}

replace_db()
{
	dbname=$1
	dbuser=$2
	dbpwd=$3

	replace "s+%DBNAME%+$dbname+" docker-compose.yml
	replace "s+%DBUSER%+$dbuser+" docker-compose.yml
	replace "s+%DBPWD%+$dbpwd+" docker-compose.yml
}

add_to_library()
{
	col_one=$(printf '%-40s' " $1")
	col_two=$(printf '%-40s' " $2")
	col_three=$(printf '%-40s' " $3")
	col_four=$(printf '%-40s' " $4")
	echo "$col_one|$col_two|$col_three|$col_four" >> "$PREFERENCESDIR/.library"
}

print_library()
{
	col_one=$(printf '%-40s' "App Name")
	col_two=$(printf '%-39s' "Path")
	col_three=$(printf '%-39s' "Port")
	col_four=$(printf '%-39s' "App Type")
	echo "$col_one|  $col_two|  $col_three|  $col_four"
	cat "$PREFERENCESDIR/.library"
}

hex_color()
{
    echo "16 + $1 * 36 + $2 * 6 + $3" | bc                        
}   

##############################
######## String Manipulation
##############################
lowercase()
{
	string=$1
	echo "$string" | dd conv=lcase 2> /dev/null
}

remove_spaces()
{
	STRING=$1
	echo $STRING | tr -s ' ' | tr ' ' '_'
}

replace()
{
	FILE=$2
	PATTERN=$1

	if [[ "$OSTYPE" == "linux-gnu"* ]]; then
		sed -i "$PATTERN" "$FILE"
	elif [[ "$OSTYPE" == "darwin"* ]]; then
		sed -i.bak "$PATTERN" "$FILE"
	fi
}

##############################
##### Output Text Manipulation
##############################
get_style_code()
{
	STYLE=$1
	case $STYLE in
		italic)
			styleInt="3"
			;;
		dark)
			styleInt="2"
			;;
		underline)
			styleInt="4"
			;;
		*)
			styleInt="0"
			;;
	esac
	echo $styleInt
}

get_color_code()
{
	COLOR=$1
	case $COLOR in
		red)
			code=31
			;;
		green)
			code=32
			;;
		yellow)
			code=33
			;;
		blue)
			code=34
			;;
		purple)
			code=35
			;;
		cyan)
			code=36
			;;
		white)
			code=37
			;;
		*)
			code="$COLOR"
			;;
	esac

	echo $code
}

color_text()
{
	COLOR=$1
	TEXT=$2
	STYLE=$3
	# BACKGROUND=$4

	suffix="\033[m"

	code=$(get_color_code $COLOR)
	styleInt=$(get_style_code $STYLE)

	#to end the string
	code=$code"m"
	
	prefix="\033[$styleInt;$code"

	echo $prefix$TEXT$suffix
}

##############################
##### Docker containers Setup
##############################
setup_nginx()
{	
	color_text green "*******************************"
	color_text green "********** Setting up nginx ***"
	color_text green "*******************************"
	color_text green "::Configuring nginx" underline

	NGINXCONFPATH="./.docker/nginx/conf.d/site.conf"

	mkdir ./.docker/nginx && mkdir ./.docker/nginx/conf.d

	color_text green "::Copying site config file"
	cp "$PREFERENCESDIR/templates/nginx/conf.d/$APPTYPE.conf" "$NGINXCONFPATH"

	color_text green "Enter host name (empty for localhost)"
	read hostname

	if [[ ! -z "$hostname" ]]; then
		color_text green "::Setting server_name"
		replace "s+server_name example.com;+server_name $hostname www.$hostname;+" "$NGINXCONFPATH"
	fi
}

setup_php()
{
	color_text purple "*******************************"
	color_text purple "************ Setting up PHP ***"
	color_text purple "*******************************"

	color_text purple "Configuring php" underline

	cp -r "$PREFERENCESDIR/templates/php" ./.docker/ 

	color_text purple "Creating docker-compose file"
	cp "$PREFERENCESDIR/templates/docker/php.yml" ./docker-compose.yml

	color_text purple "Configuring app and database in docker-compose.yml"
	replace_app_root "./"
	replace_app_port $APPPORT
	replace_db $DBNAME $DBUSER $DBPWD

	replace "s+%PHP_FOLDER%+./.docker/php+" ./docker-compose.yml

	color_text purple "Which PHP Version? (Defaults to 7.4)"
	read PHP_VERSION
	if [[ -z $PHP_VERSION ]]; then
		PHP_VERSION="7.4"
	fi
	replace "s+%PHP_VERSION%+$PHP_VERSION+" ./.docker/php/Dockerfile

	if [[ ! -z "$PHPPACKAGE" ]]; then
		write_file "RUN docker-php-ext-install $PHPPACKAGE" /.docker/php/Dockerfile 		
	fi
}

setup_lemp()
{
	color_text red "*******************************"
	color_text red "***** Setting up LEMP Stack ***"
	color_text red "*******************************"

	color_text red "Configuring LEMP stack" underline
	mkdir .docker

	setup_nginx

	setup_php
}

##############################
######### App specific setups
##############################
setup_php_app()
{
	color_text purple "*******************************"
	color_text purple "*******  Setting up PHP app ***"
	color_text purple "*******************************"
	setup_lemp

	color_text $(hex_color 5 3 0) "Choose Framework"
	color_text purple "[1] Laravel"
	color_text purple "[2] Autre"

	read framework

	case $framework in
		"1")
			setup_laravel
			;;
		*)
			;;
	esac
}

setup_wordpress()
{
	color_text blue "*******************************"
	color_text blue "********* Setting up WP app ***"
	color_text blue "*******************************"
	color_text blue "Beginning Wordpress configuration"

	skipInstall=$1

	if [[ -z $skipInstall ]]; then
		git init
		git remote add origin https://github.com/WordPress/WordPress.git

		if [[ -z $WP_VERSION || ! $WP_VERSION ]]; then
			color_text blue "Which Wordpress version do you wish to use? (empty for latest) "
			read WP_VERSION
			if [[ -z "$WP_VERSION" ]]; then
				WP_VERSION="master"
			fi
			write_site_env "WP_VERSION" "$WP_VERSION"
		fi

		color_text purple "Downloading Wordpress ($WP_VERSION)"

		#Refactor this
		if [[ $WP_VERSION == "master" ]]; then
			git pull origin master
		else
			git fetch origin "refs/tags/$WP_VERSION"
			git pull origin "tags/$WP_VERSION"
		fi

		echo ""
		rm -rf .git
	fi
	# cp "$PREFERENCESDIR/templates/wordpress/.htaccess" ./.htaccess

	color_text blue "Setting up database connection information in wp-config.php"
	mv ./wp-config-sample.php ./wp-config.php 

	replace "s|define( *'DB_NAME', *'.*' *);|define('DB_NAME', '$DBNAME');|" "./wp-config.php"
	replace "s|define( *'DB_USER' *, *'.*' *);|define('DB_USER', '$DBUSER');|" "./wp-config.php"
	replace "s|define( *'DB_PASSWORD' *, *'.*' *);|define('DB_PASSWORD', '$DBPWD');|" "./wp-config.php"
	replace "s|define( *'DB_HOST' *, *'.*' *);|define('DB_HOST', 'mysql');|" "./wp-config.php"

	setup_lemp $skipInstall
}

setup_laravel()
{
	color_text red "*******************************"
	color_text red "***  Setting up Laravel app ***"
	color_text red "*******************************"
	color_text red "Installing Laravel"

	#Because they have a docker-composer ??
	mv docker-compose.yml laravel-temp-compose.yml

	git init
	git remote add origin https://github.com/laravel/laravel.git

	if [[ -z $LARAVEL_VERSION || ! $LARAVEL_VERSION ]]; then
		color_text red "Which Laravel version do you wish to use? (empty for latest) "
		read LARAVEL_VERSION
		if [[ -z "$WP_VERSION" ]]; then
			LARAVEL_VERSION="master"
		fi
		write_site_env "LARAVEL_VERSION" "$LARAVEL_VERSION"
	fi

	color_text red "Downloading Laravel ($LARAVEL_VERSION)"

	#Refactor this
	if [[ $LARAVEL_VERSION == "master" ]]; then
		git pull origin master --force
	else
		git fetch origin "refs/tags/$LARAVEL_VERSION" 
		git pull origin "tags/$LARAVEL_VERSION" --force
	fi

	echo ""
	rm -rf .git

	composer install --prefer-dist -vv

	mv laravel-temp-compose.yml docker-compose.yml
	
	NGINXCONFPATH="./.docker/nginx/conf.d/site.conf"

	color_text red "Changin nginx root path"
	replace "s+root */var/www/html;+root /var/www/html/public;+" "$NGINXCONFPATH"

	cp .env.example .env

	replace "s+DB_HOST=localhost+DB_HOST=mysql+" .env
	replace "s+DB_DATABASE=laravel+DB_DATABASE=$DBNAME+" .env
	replace "s+DB_DATABASE=laravel+DB_DATABASE=$DBNAME+" .env
	replace "s+APP_NAME=Laravel+APP_NAME=$APPNAME+" .env

	color_text red "Setting default mail driver to Log"
	write_file "MAIL_DRIVER=log" .env

	color_text red "Generating Key"
	php artisan key:generate

	mv ./.docker/php/.dockerignore .dockerignore
}

setup_js_app()
{
	color_text yellow "*******************************"
	color_text yellow "********* Setting up JS app ***"
	color_text yellow "*******************************"

	cd $APPROOT
	color_text yellow "Configuring node" underline

	mkdir ./.docker
	mkdir ./.docker/node

	color_text yellow "Choose a framework" underline
	color_text yellow "[1] React"
	color_text yellow "[2] VueJS"
	read framework

	case $framework in 
		1)
			setup_react
			;;
		2)
			setup_vue
			;;
	esac

	color_text yellow "Configuring app in docker-compose.yml"
	replace_app_root "./"
	replace_app_port $APPPORT

	lower_name=$(lowercase "$APPNAME")
	noSpaceName=$(remove_spaces $lower_name)
	replace "s+%APPNAME%+$noSpaceName+" ./docker-compose.yml

	replace_app_port $APPPORT

	cp "$PREFERENCESDIR/templates/javascript/node/.dockerignore" ./.dockerignore
}

setup_react()
{
	color_text yellow "*******************************"
	color_text yellow "****** Setting up React app ***"
	color_text yellow "*******************************"
	
	color_text yellow "Creating docker-compose file"
	cp "$PREFERENCESDIR/templates/docker/javascript.yml" ./docker-compose.yml

	color_text yellow "Setting up React configurations"

	color_text yellow "Copying React Dockerfile"
	cp $PREFERENCESDIR/templates/javascript/node/react ./Dockerfile

	color_text yellow "Done setuping base react app configurations"
	
	color_text yellow "Installing base app"

	#todo check if yarn is installed
	yarn global add create-react-app@3.4.1
	yarn create react-app app
	mv app/* ./

	yarn global add react-scripts@3.4.1 --silent

	color_text yellow "Done installing"

	# mv ./.docker/node/react ./.docker/node/Dockerfile
}

setup_vue() 
{
	color_text green "*******************************"
	color_text green "******** Setting up VUE app ***"
	color_text green "*******************************"

	color_text green "Creating docker-compose file"
	cp "$PREFERENCESDIR/templates/docker/vue.yml" ./docker-compose.yml

	color_text green "Setting up VueJS configurations"

	color_text green "Copying VueJS Dockerfile"
	cp $PREFERENCESDIR/templates/javascript/node/vue Dockerfile

	color_text green "Done setuping base vue app configurations"
	color_text green "Installing base app"

	yarn global add @vue/cli
	vue create -d .
	
	color_text green "Done installing"
}

setup_go_app()
{
	color_text white "*******************************"
	color_text white "********* Setting up Go app ***"
	color_text white "*******************************"

	color_text white "Creating docker-compose file"
	cp "$PREFERENCESDIR/templates/docker/go.yml" ./docker-compose.yml

	color_text white "Copying Golang Dockerfile"
	cp $PREFERENCESDIR/templates/go/Dockerfile Dockerfile

	lower_name=$(lowercase "$APPNAME")
	noSpaceName=$(remove_spaces $lower_name)
	replace "s+%APPNAME%+$noSpaceName+" ./docker-compose.yml

	replace_app_port $APPPORT

	if [[ ! -f "main.go" ]]; then
		cp "$PREFERENCESDIR/templates/go/entry.go" ./main.go
	fi

	color_text white "Done installing"
}

setup_site()
{
	TYPE=$1
	SKIPINSTALL=$2
	if [[ "$TYPE" == "wordpress" ]]; then
		setup_wordpress $SKIPINSTALL
	elif [[ "$TYPE" == "php" ]]; then
		setup_php_app $SKIPINSTALL
	elif [[ "$TYPE" == "javascript" ]]; then
		setup_js_app $SKIPINSTALL
	elif [[ "$TYPE" == "go" ]]; then
		setup_go_app $SKIPINSTALL
	fi
}

go_to_hive()
{
	hive=$1

	hives=$(grep -c $hive $PREFERENCESDIR/.library)
	if [[ $hives > 1 ]]; then
		grep $hive $PREFERENCESDIR/.library
		echo "Choose proper path"
		exit
	else
		hives=$(grep $hive $PREFERENCESDIR/.library)
		IFS='|' read -ra ADDR <<< "$hives"
		path="${ADDR[1]}";
		path=$path|xargs
		echo $path
		exit
		# cd $path
		# exec sh
		# exit
		# for i in "${ADDR[@]}"; do
		#     echo $i
		# done
	fi
	exit
}


use_existing_file()
{
	## If a .site-env already exists, we ask if we want to use this file
	if [[ -f "$DIR/.site-env" ]]; then
		color_text red "Use existing .site-env file to configure app? (y/N)" > /dev/stderr 
		read existingfile
		existingfile=$(lowercase $existingfile)
		if [[ -z $existingfile ]]; then
			existingfile='n'
		fi
	else 
		existingfile='n'
	fi

	echo $existingfile
}

templates_only()
{
	existingfile=$(use_existing_file)
	if [[ ! $existingfile == 'n' ]]; then
		source "$DIR/.site-env"
	else
		color_text red "Enter app path (empty for $DIR)"
		read -e apppath

		if [[ -z $apppath ]]; then
			apppath="$DIR"
		fi

		if [[ ! -d "$apppath" ]]; then
			read -p 'Folder does not exist, confirm creation (Y/n)?' createFolder
			if [[ $createFolder == 'y' || $createFolder == 'yes' || -z "$createFolder" ]]; then
				mkdir $apppath
			fi
		fi

		if [[ -z "$APPPORT" ]]; then
			color_text red "Enter Port (Defaults to 9000) :" underline
			read APPPORT
			if [[ -z "$APPPORT" ]]; then
				APPPORT="9000"
			fi
		fi

		path=$1
		skip_installation='n'
		if [[ "$(ls -A $apppath)" ]]; then
			color_text red "Folder exists and is not empty, do you wish to skip installation and only setup docker files? (Y/n)"
			read skip_installation
		fi

		if [[ $skip_installation == 'n' || $skip_installation == 'no' ]]; then
			color_text purple "Creating configuration file in App root"
			CONFIGFILE="$apppath/.site-env"
			touch "$CONFIGFILE"
			: > "$CONFIGFILE"

			color_text purple "Gathering app information :::"
			write_site_env "APPROOT" "$apppath"

			color_text red "Enter app name (no space): "
			read name
			if [[ -z "$name" ]]; then
				color_text red "Enter app name (no space): "
				read name
			fi

			write_site_env "APPNAME" "$name"
			echo "Database configuration"

			dbname=$(lowercase $name)
			write_site_env "DBNAME" $dbname
			write_site_env "DBUSER" $dbname
			write_site_env "DBPWD" $dbname
		fi
	fi

	if [[ -z "$APPTYPE" ]]; then
		sitetype=$(get_site_type)
	fi

	cd $APPROOT
	setup_site $sitetype 'y'

	add_to_library $APPNAME $APPROOT $APPPORT $APPTYPE

	color_text blue "Do you wish to boot the app right now? (Y/n)"
	read bootapp

	# clean up
	rm Dockerfile.bak

	if [[ "$bootapp" == 'y' || $bootapp == 'yes' || -z $bootapp ]]; then
		color_text blue "Building containers"
		docker-compose build
		docker-compose up -d
	else
		clear
		color_text red "!! Remember to build your containers before booting them !!"
	fi

	echo ""

	color_text green "*******************************"
	color_text green "***************** App setup ***"
	color_text green "*******************************"

	color_text green "Successfully created $APPNAME | $APPPORT | $APPROOT | $APPTYPE"

	exit
}

get_site_type()
{
	sitetypes=("" "wordpress" "php" "javascript" "hybrid" "go")

	echo "Enter site type (Defaults to 1)" > /dev/stderr
	color_text blue "[1] Wordpress" > /dev/stderr
	color_text purple "[2] PHP app (Laravel, vanilla, etc.)" > /dev/stderr
	color_text yellow "[3] Javascript app (React, VueJS, etc.)" > /dev/stderr
	color_text red "[4] Hybrid (Javascript app with Backend API)" > /dev/stderr
	color_text white "[5] Golang" > /dev/stderr
	read -p "Enter site type(1): " sitetype > /dev/stderr
	if [[ -z "$sitetype" ]]; then
		sitetype=1
	fi

	echo "${sitetypes[$sitetype]}"
}

######################################
#######						 #########
#######	    APP IN ITSELF    #########
#######						 #########
######################################
if [[ $WHOAMI == "root" ]]; then
	color_text red "Hivemind should not be run as root."
	exit
fi

ACTION=$1
if [[ ! -z "$ACTION" ]]; then
	case $ACTION in
		"list")
			print_library
			exit
			;;
		"stopall")
			docker stop $(docker ps -a -q)
			exit
			;;
		"rmall")
			docker rm $(docker ps -a -q)
			exit
			;;
		"go")
			go_to_hive $2
			;;
		"tpl")
			templates_only
			exit
			;;
		*)
			;;
	esac
fi

existingfile=$(use_existing_file)

# If the user doesn't want to use an existing .site-env file
if [[ $existingfile == 'n' || $existingfile == 'no' ]]; then
	color_text red "Enter app path (empty for $DIR)"
	read -e apppath

	if [[ -z $apppath ]]; then
		apppath="$DIR"
	fi

	if [[ ! -d "$apppath" ]]; then
		read -p 'Folder does not exist, confirm creation (Y/n)?' createFolder
		if [[ $createFolder == 'y' || $createFolder == 'yes' || -z "$createFolder" ]]; then
			mkdir $apppath
		fi
	fi

	path=$1
	skip_installation='n'
	# if [[ "$(ls -A $apppath)" ]]; then
	# 	color_text red "Folder exists and is not empty, do you wish to skip installation and only setup docker files? (Y/n)"
	# 	read skip_installation
	# fi

	if [[ $skip_installation == 'n' || $skip_installation == 'no' ]]; then
		color_text purple "Creating configuration file in App root"
		CONFIGFILE="$apppath/.site-env"
		touch "$CONFIGFILE"
		: > "$CONFIGFILE"

		color_text purple "Gathering app information :::"
		write_site_env "APPROOT" "$apppath"

		color_text red "Enter app name (no space): "
		read name
		if [[ -z "$name" ]]; then
			color_text red "Enter app name (no space): "
			read name
		fi

		write_site_env "APPNAME" "$name"
		echo "Database configuration"

		dbname=$(lowercase $name)
		write_site_env "DBNAME" $dbname
		write_site_env "DBUSER" $dbname
		write_site_env "DBPWD" $dbname
	else
		CONFIGFILE="$apppath/.site-env"	
	fi
else
	CONFIGFILE="$DIR/.site-env"	
fi

#Load base config file into VAR=VALUE
source $CONFIGFILE

if [[ ! $APPROOT = ./* && ! $APPROOT = /* ]]; then
	color_text red "Path must be relative(starting with ./) or absolute (starting with /)"
	exit
fi

if [[ $existingfile == 'n' || $existingfile == 'no' || -z "$APPTYPE" ]]; then
	sitetype=$(get_site_type)
	write_site_env "APPTYPE" $sitetype
elif [[ -z "$APPTYPE" ]]; then
	sitetype="wordpress"
	write_site_env "APPTYPE" "$sitetype"
fi


if [[ -z "$APPPORT" ]]; then
	color_text red "Enter Port (Defaults to 9000) :" underline
	read APPPORT
	if [[ -z "$APPPORT" ]]; then
		APPPORT="9000"
	fi
fi

if [[ $skip_installation == 'n' ]]; then
	color_text red "Are you sure you wish to create a $APPTYPE app on port $APPPORT in folder $APPROOT? (Y/n)"
	read confirm
fi


if [[ "$confirm" == "y" || "$confirm" == "yes" || -z "$confirm" ]]; then
	color_text green "Initializing"
else
	color_text red 'Exiting'
	exit;
fi

if [[ ! -d $APPROOT ]]; then
	mkdir $APPROOT
fi

cd $APPROOT
setup_site $sitetype

add_to_library $APPNAME $APPROOT $APPPORT $APPTYPE

color_text blue "Do you wish to boot the app right now? (Y/n)"
read bootapp

# clean up
rm Dockerfile.bak

if [[ "$bootapp" == 'y' || $bootapp == 'yes' || -z $bootapp ]]; then
	color_text blue "Building containers"
	docker-compose build
	docker-compose up -d
else
	clear
	color_text red "!! Remember to build your containers before booting them !!"
fi

echo ""

color_text green "*******************************"
color_text green "***************** App setup ***"
color_text green "*******************************"

color_text green "Successfully created $APPNAME | $APPPORT | $APPROOT | $APPTYPE"

exit
#docker stop $(docker ps -a -q)
#docker rm $(docker ps -a -q)