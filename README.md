# Hivemind

## Installation

Clone git and run `sudo sh install-hivemind.sh`

Note: HTTPS support will be added soon

## Usage

By default the prompt is interactive, so you may run simply `hivemind` and go from there. If you run the `hivemind` in a folder with an existing .site-env, it will prompt you if you want to use it.

### Interactive configuration

- Enter app path, the script can create it, if it doesn't exist.
- App Name: The app name that will be used to generate default credentials and such
- For now only the Wordpress options works
- Enter port (defaults to 9000, use 80 if you want to use vhost)


## Other functions

`list` Returns a list of all the apps created with hivemind. Provides the name, path, port and type.

`tpl` Creates only the docker files, **does not install any framework**

ex: `hivemind tpl`

## To do

- Implement a couple of checks to insure specific errors are thrown
- Implement making an existing project into a docker project
- Implement more framewoek
- Implement a way to leverage more configurations
